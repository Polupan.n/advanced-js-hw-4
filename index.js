"use strict";

// ## Теоретичне питання
// Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.

// AJAX (Asynchronous JavaScript and XML) - це технологія, що дозволяє веб-сторінкам взаємодіяти з сервером без необхідності перезавантаження сторінки. За допомогою AJAX можна відправляти та отримувати дані з сервера асинхронно, тобто без очікування завершення операції.

// Основні переваги використання AJAX у розробці JavaScript:

// 1. Асинхронність: Запити до сервера виконуються асинхронно, тому користувач може продовжувати взаємодію зі сторінкою, не чекаючи завершення запиту.

// 2. Оновлення частин сторінки: AJAX дозволяє оновлювати тільки певні частини сторінки, не перезавантажуючи її цілком. Це робить веб-сторінку більш динамічною та швидшою.

// 3. Менша пропускна здатність: Оскільки AJAX дозволяє передавати лише необхідні дані, використовуючи асинхронні запити, це може призвести до зменшення обсягу переданих даних і зниження пропускної здатності.

// 4. Взаємодія з сервером: Завдяки AJAX можна взаємодіяти з сервером, відправляти дані на нього і отримувати відповіді без перезавантаження сторінки, що дозволяє створювати більш інтерактивні та зручні веб-додатки.

// ## Завдання
// Отримати список фільмів серії `Зоряні війни` та вивести на екран список персонажів для кожного з них.

// #### Технічні вимоги:
// - Надіслати AJAX запит на адресу `https://ajax.test-danit.com/api/swapi/films` та отримати список усіх фільмів серії `Зоряні війни`
// - Для кожного фільму отримати з сервера список персонажів, які були показані у цьому фільмі. Список персонажів можна отримати з властивості `characters`.
// - Як тільки з сервера буде отримана інформація про фільми, відразу вивести список усіх фільмів на екрані. Необхідно вказати номер епізоду, назву фільму, а також короткий зміст (поля `episodeId`, `name`, `openingCrawl`).
// - Як тільки з сервера буде отримано інформацію про персонажів будь-якого фільму, вивести цю інформацію на екран під назвою фільму.

// #### Необов'язкове завдання підвищеної складності
//  - Поки завантажуються персонажі фільму, прокручувати під назвою фільму анімацію завантаження. Анімацію можна використовувати будь-яку. Бажано знайти варіант на чистому CSS без використання JavaScript.

// #### Література:
// - [Використання Fetch на MDN](https://developer.mozilla.org/ru/docs/Web/API/Fetch_API/Using_Fetch)
// - [Fetch](https://learn.javascript.ru/fetch)
// - [CSS анімація](https://html5book.ru/css3-animation/)
// - [Події DOM](https://learn.javascript.ru/introduction-browser-events)

const URL = "https://ajax.test-danit.com/api/swapi/films";
const root = document.querySelector("#root");

class Film {
  request(url) {
    return fetch(url).then((response) => {
      return response.json();
    });
  }

  getFilms(url) {
    return this.request(url);
  }

  getCharacters(characters) {
    const charactersRequests = characters.map((characters) => {
      return this.request(characters);
    });

    return Promise.allSettled([...charactersRequests]);
  }

  renderFilms(films) {
    let filmsContainer = document.createElement("div");
    let fragment = document.createDocumentFragment();
    films.map(({ episodeId, name, openingCrawl, characters }, index) => {
      let filmEpisodeId = document.createElement("p");
      let filmName = document.createElement("p");
      let filmOpeningCrawl = document.createElement("p");
      let charactersList = document.createElement("ul");

      filmEpisodeId.textContent = episodeId;
      filmName.textContent = name;
      filmOpeningCrawl.textContent = openingCrawl;
      fragment.append(
        filmEpisodeId,
        filmName,
        filmOpeningCrawl,
        charactersList
      );

      setTimeout(() => {
        this.getCharacters(characters).then((characters) => {
          let charactersItems = characters.map(({ value }) => {
            let li = document.createElement("li");
            li.textContent = value.name;
            return li;
          });
          charactersList.append(...charactersItems);
          return charactersList;
        });
      }, 1000 * index);

      return fragment;
    });

    filmsContainer.append(fragment);
    return filmsContainer;
  }
}

let film = new Film();

film
  .getFilms(URL)
  .then((films) => {
    console.log(films);
    root.append(film.renderFilms(films));
  })
  .catch((e) => {
    console.log(e);
  });
